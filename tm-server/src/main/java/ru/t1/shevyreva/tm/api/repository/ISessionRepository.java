package ru.t1.shevyreva.tm.api.repository;

import ru.t1.shevyreva.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
