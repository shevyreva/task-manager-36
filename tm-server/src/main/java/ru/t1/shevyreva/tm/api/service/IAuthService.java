package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Session;
import ru.t1.shevyreva.tm.model.User;

import javax.security.sasl.AuthenticationException;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User check(@Nullable String password, @Nullable String login) throws AuthenticationException;

    @NotNull
    String login(@Nullable final String login, @Nullable final String password);

    @NotNull
    Session validateToken(@Nullable final String token);

    void invalidate(@Nullable final Session session);

}
