package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.exception.field.EmailEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.LoginEmptyException;
import ru.t1.shevyreva.tm.exception.field.PasswordEmptyException;
import ru.t1.shevyreva.tm.exception.user.ExistsEmailException;
import ru.t1.shevyreva.tm.exception.user.ExistsLoginException;
import ru.t1.shevyreva.tm.exception.user.RoleEmptyException;
import ru.t1.shevyreva.tm.model.User;
import ru.t1.shevyreva.tm.repository.ProjectRepository;
import ru.t1.shevyreva.tm.repository.TaskRepository;
import ru.t1.shevyreva.tm.repository.UserRepository;

public class UserServiceTest {

    private static final int NUMBER_OF_USER = 3;

    @NotNull IUserRepository userRepository;

    @NotNull IProjectRepository projectRepository = new ProjectRepository();

    @NotNull ITaskRepository taskRepository = new TaskRepository();

    @NotNull IUserService userService;

    @NotNull IPropertyService propertyService = new PropertyService();

    @Before
    public void initService() {
        System.out.println("BEFORE");
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_USER; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            user.setEmail("email " + i);
            userRepository.add(user);
        }
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
    }

    @Test
    public void testCreate() {
        final int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userService.getSize());
        @NotNull final String login = "newUser";
        @NotNull final String password = "newPassword";
        @NotNull final User user = userService.create(login, password);

        Assert.assertEquals(login, user.getLogin());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, password));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("new", null));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user 1", password));

        Assert.assertEquals(expectedCount, userService.getSize());
    }

    @Test
    public void testCreateWithEmail() {
        final int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userService.getSize());
        @NotNull final String login = "newUser";
        @NotNull final String password = "newPassword";
        @NotNull final String email = "email";
        @NotNull final User user = userService.create(login, password, email);

        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(email, user.getEmail());

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, password, "22"));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("login", null, "33"));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user 1", password, "444"));
        Assert.assertThrows(ExistsEmailException.class, () -> userService.create("login2", password, email));

        Assert.assertEquals(expectedCount, userService.getSize());
    }

    @Test
    public void testCreateWithRole() {
        final int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userService.getSize());
        @NotNull final String login = "newUser";
        @NotNull final String password = "newPassword";
        @NotNull final Role role = Role.USUAL;
        @NotNull final User user = userService.create(login, password, role);

        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(role, user.getRole());

        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, password, role));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create("1", null, role));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user 1", password, role));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create("login2", password, (Role) null));

        Assert.assertEquals(expectedCount, userService.getSize());
    }

    @Test
    public void testFindByLogin() {
        @NotNull final String login = "user 1";
        @NotNull final String email = "email 1";
        @NotNull final String incorrectLogin = "user 555";
        @NotNull final User user = userService.findByLogin(login);

        Assert.assertNotNull(user);
        Assert.assertEquals(email, user.getEmail());
        Assert.assertEquals(login, user.getLogin());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByLogin(incorrectLogin));
    }

    @Test
    public void testFindByEmail() {
        @NotNull final String login = "user 1";
        @NotNull final String email = "email 1";
        @NotNull final String incorrectEmail = "email 555";
        @NotNull final User user = userService.findByEmail(email);

        Assert.assertNotNull(user);
        Assert.assertEquals(email, user.getEmail());
        Assert.assertEquals(login, user.getLogin());
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.findByEmail(incorrectEmail));
    }

    @Test
    public void testLockUser() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userService.add(newUser);

        userService.lockUser(login);
        Assert.assertTrue(newUser.getLocked());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUser(null));
    }

    @Test
    public void testUnlockUser() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userService.add(newUser);

        userService.unlockUser(login);
        Assert.assertFalse(newUser.getLocked());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUser(null));
    }

    @Test
    public void testLoginExist() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userService.add(newUser);

        Assert.assertTrue(userService.isLoginExist(login));
        Assert.assertFalse(userService.isLoginExist("notExist"));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.isLoginExist(null));
    }

    @Test
    public void testEmailExist() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userService.add(newUser);

        Assert.assertTrue(userService.isEmailExist(email));
        Assert.assertFalse(userService.isEmailExist("notExist"));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.isEmailExist(null));
    }

    @Test
    public void testRemoveOne() {
        int expectedCount = NUMBER_OF_USER - 1;
        Assert.assertEquals(NUMBER_OF_USER, userService.getSize());

        @NotNull final User userForRemove = userRepository.findOneByIndex(0);
        @NotNull final String login = userForRemove.getLogin();
        @NotNull final String email = userForRemove.getEmail();
        @NotNull final String userId = userForRemove.getId();
        userService.removeOne(userForRemove);

        Assert.assertFalse(userService.isLoginExist(login));
        Assert.assertFalse(userService.isEmailExist(email));
        Assert.assertFalse(userService.existsById(userId));
        Assert.assertEquals(expectedCount, userService.getSize());
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeOne(null));
    }

    @Test
    public void setPassword() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String password = "password";
        @NotNull final String userId = newUser.getId();
        userService.add(newUser);

        userService.setPassword(userId, password);
        Assert.assertNotNull(newUser.getPasswordHash());
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(null, password));
    }

    @Test
    public void updateUser() {
        @NotNull final String newFirstName = "FirstName";
        @NotNull final String newLastName = "LastName";
        @NotNull final String newMiddleName = "MiddleName";
        @NotNull final User user = userService.findOneByIndex(0);
        @NotNull final String userId = user.getId();

        userService.updateUser(userId, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(null, newFirstName, newMiddleName, newLastName));
    }

    @After
    public void testRemoveAll() {
        System.out.println("AFTER");
        userService.removeAll();
        Assert.assertEquals(0, userService.getSize());
    }

}
