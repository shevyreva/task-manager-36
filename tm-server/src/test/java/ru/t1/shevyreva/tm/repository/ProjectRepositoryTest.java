package ru.t1.shevyreva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.model.Project;

import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        System.out.println("BEFORE");
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project " + i);
            project.setDescription("description" + i);
            if (i < 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
        }
    }

    @Test
    public void testAddProject() {
        int expectedCount = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project newProject = new Project();
        projectRepository.add(newProject);
        Assert.assertEquals(expectedCount, projectRepository.getSize());
    }

    @Test
    public void testAddActualProject() {
        int expectedCount = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "ActualName";
        @NotNull final String description = "ActualDescription";
        @NotNull final Project project = new Project();

        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        Assert.assertEquals(expectedCount, projectRepository.getSize());

        @Nullable final Project actualProject = projectRepository.findOneByIndex(projectRepository.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void testCreateProject() {
        int expectedCount = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull Project project_one = projectRepository.create(userId, "ProjectOne");
        Assert.assertEquals(expectedCount, projectRepository.findAll().size());

        @NotNull Project project_two = projectRepository.create(userId, "ProjectTwo", "ProjectDescription");
        Assert.assertEquals(expectedCount + 1, projectRepository.getSize());

        @NotNull final String expectedName = project_one.getName();
        @NotNull final String actualProjectOneName = projectRepository.findOneById(project_one.getId()).getName();
        Assert.assertEquals(expectedName, actualProjectOneName);

        @NotNull final String expectedDescription = project_two.getDescription();
        @NotNull final String actualProjectTwoDescription = projectRepository.findOneById(project_two.getId()).getDescription();
        Assert.assertEquals(expectedDescription, actualProjectTwoDescription);
    }

    @Test
    public void testFindAll() {
        int actualSize = projectRepository.findAll().size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualSize);

        @NotNull final Project project = new Project();
        project.setName("FindAll");
        project.setDescription("FindAll");
        projectRepository.add(project);

        int expectedCount = NUMBER_OF_ENTRIES + 1;
        actualSize = projectRepository.getSize();
        Assert.assertEquals(expectedCount, actualSize);
    }

    @Test
    public void testFindAllComparator() {
        int actualSizeOne = projectRepository.findAll(ProjectSort.BY_NAME.getComparator()).size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualSizeOne);
        int actualSizeTwo = projectRepository.findAll(ProjectSort.BY_CREATED.getComparator()).size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualSizeTwo);
        int actualSizeThree = projectRepository.findAll(ProjectSort.BY_STATUS.getComparator()).size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualSizeThree);
    }

    @Test
    public void testFindByProjectId() {
        @NotNull Project newProject = new Project();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "FindById";
        @NotNull final String description = "FindById";
        @NotNull final String id = newProject.getId();
        @NotNull final String incorrectId = UUID.randomUUID().toString();

        newProject.setName(name);
        newProject.setUserId(userId);
        newProject.setDescription(description);
        projectRepository.add(newProject);

        @NotNull final Project findProjectById = projectRepository.findOneById(id);
        @NotNull final Project findProjectByUserId = projectRepository.findOneById(userId, id);
        Assert.assertNotNull(findProjectById);
        Assert.assertNotNull(findProjectByUserId);

        @NotNull final String expectedName = newProject.getName();
        Assert.assertEquals(expectedName, findProjectById.getName());
        Assert.assertEquals(expectedName, findProjectByUserId.getName());

        @NotNull final Project findIncorrectById = projectRepository.findOneById(incorrectId);
        @NotNull final Project findIncorrectByUserId = projectRepository.findOneById(userId, incorrectId);
        Assert.assertNull(findIncorrectById);
        Assert.assertNull(findIncorrectByUserId);
    }

    @Test
    public void testFindByProjectIndex() {
        @NotNull Project newProject = new Project();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "FindByIndex";
        @NotNull final String description = "FindByIndex";

        newProject.setName(name);
        newProject.setUserId(userId);
        newProject.setDescription(description);
        projectRepository.add(newProject);

        @NotNull final Integer index = NUMBER_OF_ENTRIES;
        @NotNull final Project findProjectByIndex = projectRepository.findOneByIndex(index);
        @NotNull final Project findProjectByUserId = projectRepository.findOneByIndex(userId, 0);
        Assert.assertNotNull(findProjectByIndex);
        Assert.assertNotNull(findProjectByUserId);

        @NotNull final String expectedName = newProject.getName();
        Assert.assertEquals(expectedName, findProjectByIndex.getName());
        Assert.assertEquals(expectedName, findProjectByUserId.getName());
    }

    @Test
    public void testFindForUser() {
        Assert.assertEquals(4, projectRepository.findAll(USER_ID_1).size());
        Assert.assertEquals(6, projectRepository.findAll(USER_ID_2).size());

        @NotNull Project newProject = new Project();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final String name = "FindByUser";
        @NotNull final String description = "FindByUser";

        newProject.setUserId(userId);
        newProject.setName(name);
        newProject.setDescription(description);
        projectRepository.add(newProject);

        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        for (@NotNull final Project project : projects) {
            Assert.assertEquals(newProject.getUserId(), project.getUserId());
        }

        @NotNull final List<Project> incorrectProjects = projectRepository.findAll(incorrectUserId);
        for (@NotNull final Project project : incorrectProjects) {
            Assert.assertEquals(newProject.getUserId(), project.getUserId());
        }
    }

    @Test
    public void testRemoveOne() {
        @NotNull Project findProject = projectRepository.findOneByIndex(0);
        int expected = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
        projectRepository.removeOne(findProject);
        Assert.assertEquals(expected, projectRepository.getSize());
        Assert.assertNull(projectRepository.findOneById(findProject.getId()));
    }

    @Test
    public void testRemoveById() {
        @NotNull Project findProject = projectRepository.findOneByIndex(0);
        int expected = NUMBER_OF_ENTRIES - 1;
        @NotNull final String id = findProject.getId();
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
        projectRepository.removeOneById(id);
        Assert.assertEquals(expected, projectRepository.getSize());
        Assert.assertNull(projectRepository.findOneById(id));
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull Project findProject = projectRepository.findOneByIndex(0);
        int expected = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
        projectRepository.removeOneByIndex(0);
        Assert.assertEquals(expected, projectRepository.getSize());
        Assert.assertNull(projectRepository.findOneById(findProject.getId()));
    }

    @Test
    public void testRemoveForUser() {
        int userProjectCount = projectRepository.findAll(USER_ID_1).size();
        int expected = NUMBER_OF_ENTRIES - userProjectCount;
        @Nullable final List<Project> projects = projectRepository.findAll(USER_ID_1);
        projectRepository.removeAll(USER_ID_1);
        for (@NotNull final Project project : projects) {
            Assert.assertNull(projectRepository.findOneById(project.getId()));
        }
        Assert.assertEquals(expected, projectRepository.getSize());
    }

    @After
    public void testClearProjects() {
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
        System.out.println("AFTER");
    }

}

