package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

public class TaskServiceTest {


    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private ITaskService taskService;

    @Before
    public void initService() {
        System.out.println("BEFORE");
        taskRepository = new TaskRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task " + i);
            task.setDescription("description" + i);
            if (i < 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            taskRepository.add(task);
        }
        taskService = new TaskService(taskRepository);
    }

    @Test
    public void testAddTask() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES + 1;

        @NotNull final String name = "task";
        @NotNull final String description = "description";
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Task task = new Task();

        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskService.add(userId, task);

        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(null, task));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.add(userId, null));

        Assert.assertEquals(expectedCount, taskService.getSize());
    }

    @Test
    public void testCreateTask() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "task";
        @NotNull final String description = "description";
        @NotNull final String userId = UUID.randomUUID().toString();
        taskService.create(userId, name, description);

        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(null, name, description));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(userId, null, description));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(userId, name, null));

        Assert.assertEquals(expectedCount, taskService.getSize());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.findAll().size());
        final int actualOne = taskService.findAll(TaskSort.BY_NAME.getComparator()).size();
        final int actualTwo = taskService.findAll(TaskSort.BY_CREATED.getComparator()).size();
        final int actualThree = taskService.findAll(TaskSort.BY_STATUS.getComparator()).size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualTwo);
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualOne);
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualThree);
    }

    @Test
    public void testFindAllForUser() {
        final int userCount = 4;
        Assert.assertEquals(userCount, taskService.findAll(USER_ID_1).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAll((String) null));
    }

    @Test
    public void testFindByTaskId() {
        @NotNull final Task newTask = new Task();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final String taskId = newTask.getId();
        @NotNull final String incorrectTaskId = UUID.randomUUID().toString();

        newTask.setUserId(userId);
        newTask.setName("newTask");
        newTask.setDescription("newDescription");
        taskService.add(newTask);
        Assert.assertNotNull(taskService.findOneById(userId, taskId));

        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneById(null, taskId));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(userId, null));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(incorrectUserId, taskId));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(userId, incorrectTaskId));
    }

    @Test
    public void testFindByTaskIndex() {
        @NotNull final Task newTask = new Task();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();

        newTask.setUserId(userId);
        newTask.setName("newTask");
        newTask.setDescription("newDescription");
        taskService.add(newTask);
        @NotNull final Integer index = 0;
        @NotNull final Integer incorrectIndex = 666;
        Assert.assertNotNull(taskService.findOneByIndex(userId, index));

        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findOneByIndex(null, index));
        Assert.assertThrows(IndexEmptyException.class, () -> taskService.findOneByIndex(userId, null));
        Assert.assertThrows(IndexEmptyException.class, () -> taskService.findOneByIndex(userId, incorrectIndex));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneByIndex(incorrectUserId, index));
    }

    @Test
    public void testFindByProjectId() {
        @NotNull final Task updateTask = taskService.findOneByIndex(0);
        @NotNull final String userId = updateTask.getUserId();
        @NotNull final String projectId = UUID.randomUUID().toString();
        updateTask.setProjectId(projectId);

        @NotNull List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        for (@NotNull final Task task : tasks) {
            Assert.assertEquals(projectId, task.getProjectId());
        }

        Assert.assertThrows(ProjectIdEmptyException.class, () -> taskService.findAllByProjectId(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(null, projectId));
    }

    @Test
    public void testRemoveOne() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull final Task removeTask = taskService.findOneByIndex(0);
        @NotNull final String id = removeTask.getId();
        taskService.removeOne(removeTask);

        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(id));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOne(null));

        Assert.assertEquals(expectedCount, taskService.getSize());
    }

    @Test
    public void testRemoveOneForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull final Task removeTask = taskService.findOneByIndex(0);
        @NotNull final String id = removeTask.getId();
        @NotNull final String userId = removeTask.getUserId();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        taskService.removeOne(userId, removeTask);

        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(id));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOne(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeOne(null, removeTask));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOne(incorrectUserId, removeTask));

        Assert.assertEquals(expectedCount, taskService.getSize());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull Task removeTask = taskRepository.findOneByIndex(0);
        @NotNull final String id = removeTask.getId();
        @NotNull final String incorrectId = UUID.randomUUID().toString();
        taskService.removeOneById(id);

        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(id));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOneById(incorrectId));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeOneById(null));

        Assert.assertEquals(expectedCount, taskService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull Task removeTask = taskRepository.findOneByIndex(0);
        @NotNull final String id = removeTask.getId();
        @NotNull final String userId = removeTask.getUserId();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final String incorrectId = UUID.randomUUID().toString();
        taskService.removeOneById(userId, id);

        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(id));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeOneById(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeOneById(null, id));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOneById(incorrectUserId, id));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOneById(userId, incorrectId));

        Assert.assertEquals(expectedCount, taskService.getSize());
    }


    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull Task removeTask = taskRepository.findOneByIndex(0);
        @NotNull final String id = removeTask.getId();
        @NotNull final Integer incorrectIndex = 666;
        taskService.removeOneByIndex(0);

        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(id));
        Assert.assertThrows(IndexEmptyException.class, () -> taskService.removeOneByIndex(null));
        Assert.assertThrows(IndexEmptyException.class, () -> taskService.removeOneByIndex(incorrectIndex));

        Assert.assertEquals(expectedCount, taskService.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull Task removeTask = taskRepository.findOneByIndex(0);
        @NotNull final String id = removeTask.getId();
        @NotNull final String userId = removeTask.getUserId();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final Integer incorrectIndex = 666;
        taskService.removeOneByIndex(0);

        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.findOneById(id));

        Assert.assertThrows(IndexEmptyException.class, () -> taskService.removeOneByIndex(userId, incorrectIndex));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.removeOneByIndex(null, 0));
        Assert.assertThrows(IndexEmptyException.class, () -> taskService.removeOneByIndex(userId, null));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.removeOneByIndex(incorrectUserId, 0));

        Assert.assertEquals(expectedCount, taskService.getSize());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newDescription";
        @NotNull final Task updateTask = taskService.findOneByIndex(0);
        @NotNull final String userId = updateTask.getUserId();
        @NotNull final String id = updateTask.getId();

        taskService.updateById(userId, id, newName, newDescription);
        Assert.assertEquals(newName, updateTask.getName());
        Assert.assertEquals(newDescription, updateTask.getDescription());

        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, null, newName, newDescription));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, id, newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, id, null, newDescription));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newDescription";
        @NotNull final Task updateTask = taskService.findOneByIndex(0);
        @NotNull final String userId = updateTask.getUserId();

        taskService.updateByIndex(userId, 0, newName, newDescription);
        Assert.assertEquals(newName, updateTask.getName());
        Assert.assertEquals(newDescription, updateTask.getDescription());

        Assert.assertThrows(IndexEmptyException.class, () -> taskService.updateByIndex(userId, null, newName, newDescription));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(null, 0, newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex(userId, 0, null, newDescription));
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final Task updateTask = taskService.findOneByIndex(0);
        @NotNull final String userId = updateTask.getUserId();
        @NotNull final String id = updateTask.getId();
        @NotNull final Status completed = Status.COMPLETED;
        @NotNull final Status progress = Status.IN_PROGRESS;
        @NotNull final Status notStarted = Status.NON_STARTED;

        taskService.changeTaskStatusById(userId, id, completed);
        Assert.assertEquals(completed, updateTask.getStatus());

        taskService.changeTaskStatusById(userId, id, progress);
        Assert.assertEquals(progress, updateTask.getStatus());

        taskService.changeTaskStatusById(userId, id, notStarted);
        Assert.assertEquals(notStarted, updateTask.getStatus());

        Assert.assertThrows(StatusNotFoundException.class, () -> taskService.changeTaskStatusById(userId, id, null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, null, completed));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, id, completed));
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final Task updateTask = taskService.findOneByIndex(0);
        @NotNull final String userId = updateTask.getUserId();
        @NotNull final Status completed = Status.COMPLETED;
        @NotNull final Status progress = Status.IN_PROGRESS;
        @NotNull final Status notStarted = Status.NON_STARTED;

        taskService.changeTaskStatusByIndex(userId, 0, completed);
        Assert.assertEquals(completed, updateTask.getStatus());

        taskService.changeTaskStatusByIndex(userId, 0, progress);
        Assert.assertEquals(progress, updateTask.getStatus());

        taskService.changeTaskStatusByIndex(userId, 0, notStarted);
        Assert.assertEquals(notStarted, updateTask.getStatus());

        Assert.assertThrows(StatusNotFoundException.class, () -> taskService.changeTaskStatusByIndex(userId, 0, null));
        Assert.assertThrows(IndexEmptyException.class, () -> taskService.changeTaskStatusByIndex(userId, null, completed));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(null, 0, completed));
    }

    @After
    public void testClearTasks() {
        System.out.println("AFTER");
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }


}
