package ru.t1.shevyreva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.model.User;

import java.util.UUID;

public class UserRepositoryTest {

    private static final int NUMBER_OF_USER = 3;

    @NotNull IUserRepository userRepository;

    @Before
    public void initRepository() {
        System.out.println("BEFORE");
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_USER; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            user.setEmail("email " + i);
            userRepository.add(user);
        }
    }

    @Test
    public void testCreateUser() {
        int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userRepository.getSize());

        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";
        @NotNull final String password = "password";

        newUser.setLogin(login);
        newUser.setEmail(email);
        newUser.setPasswordHash(password);
        userRepository.add(newUser);
        Assert.assertEquals(expectedCount, userRepository.getSize());
    }

    @Test
    public void testFindByEmail() {
        int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userRepository.getSize());

        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";
        @NotNull final String incorrectEmail = "qqq";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userRepository.add(newUser);
        Assert.assertEquals(expectedCount, userRepository.getSize());

        @NotNull final User findNewUser = userRepository.findByEmail(email);
        Assert.assertNotNull(findNewUser);
        Assert.assertEquals(email, findNewUser.getEmail());
        Assert.assertEquals(login, findNewUser.getLogin());

        @Nullable final User findUser = userRepository.findByEmail(incorrectEmail);
        Assert.assertNull(findUser);
    }

    @Test
    public void testFindByLogin() {
        int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userRepository.getSize());

        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";
        @NotNull final String incorrectLogin = "qqq";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userRepository.add(newUser);
        Assert.assertEquals(expectedCount, userRepository.getSize());

        @NotNull final User findNewUser = userRepository.findByLogin(login);
        Assert.assertNotNull(findNewUser);
        Assert.assertEquals(email, findNewUser.getEmail());
        Assert.assertEquals(login, findNewUser.getLogin());

        @Nullable final User findUser = userRepository.findByLogin(incorrectLogin);
        Assert.assertNull(findUser);
    }

    @Test
    public void testFindById() {
        int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userRepository.getSize());

        @NotNull final User newUser = new User();
        @NotNull final String userId = newUser.getId();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userRepository.add(newUser);
        Assert.assertEquals(expectedCount, userRepository.getSize());

        @NotNull final User findNewUser = userRepository.findOneById(userId);
        Assert.assertNotNull(findNewUser);
        Assert.assertEquals(email, findNewUser.getEmail());
        Assert.assertEquals(login, findNewUser.getLogin());

        @Nullable final User findUser = userRepository.findByLogin(incorrectUserId);
        Assert.assertNull(findUser);
    }

    @Test
    public void testFindByIndex() {
        int expectedCount = NUMBER_OF_USER + 1;
        Assert.assertEquals(NUMBER_OF_USER, userRepository.getSize());

        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userRepository.add(newUser);
        Assert.assertEquals(expectedCount, userRepository.getSize());

        @NotNull final Integer index = userRepository.getSize() - 1;
        @NotNull final User findNewUser = userRepository.findOneByIndex(index);
        Assert.assertNotNull(findNewUser);
        Assert.assertEquals(email, findNewUser.getEmail());
        Assert.assertEquals(login, findNewUser.getLogin());
    }

    @Test
    public void testUnlockUser() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userRepository.add(newUser);

        userRepository.lockUser(login);
        Assert.assertTrue(newUser.getLocked());
    }

    @Test
    public void testLockUser() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        newUser.setLocked(true);
        userRepository.add(newUser);

        userRepository.unlockUser(login);
        Assert.assertFalse(newUser.getLocked());
    }

    @Test
    public void testLoginExist() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userRepository.add(newUser);

        Assert.assertTrue(userRepository.isLoginExist(login));
        Assert.assertFalse(userRepository.isLoginExist("notExist"));
    }

    @Test
    public void testEmailExist() {
        @NotNull final User newUser = new User();
        @NotNull final String login = "newUser";
        @NotNull final String email = "newEmail";

        newUser.setLogin(login);
        newUser.setEmail(email);
        userRepository.add(newUser);

        Assert.assertTrue(userRepository.isEmailExist(email));
        Assert.assertFalse(userRepository.isEmailExist("notExist"));
    }

    @Test
    public void testRemoveOne() {
        int expectedCount = NUMBER_OF_USER - 1;
        Assert.assertEquals(NUMBER_OF_USER, userRepository.getSize());

        @NotNull final User userForRemove = userRepository.findOneByIndex(0);
        @NotNull final String login = userForRemove.getLogin();
        @NotNull final String email = userForRemove.getEmail();
        @NotNull final String userId = userForRemove.getId();
        userRepository.removeOne(userForRemove);

        Assert.assertFalse(userRepository.isLoginExist(login));
        Assert.assertFalse(userRepository.isEmailExist(email));
        Assert.assertFalse(userRepository.existsById(userId));
        Assert.assertEquals(expectedCount, userRepository.getSize());
    }

    @Test
    public void testRemoveById() {
        int expectedCount = NUMBER_OF_USER - 1;
        Assert.assertEquals(NUMBER_OF_USER, userRepository.getSize());

        @NotNull final User userForRemove = userRepository.findOneByIndex(0);
        @NotNull final String login = userForRemove.getLogin();
        @NotNull final String email = userForRemove.getEmail();
        @NotNull final String userId = userForRemove.getId();
        userRepository.removeOneById(userId);

        Assert.assertFalse(userRepository.isLoginExist(login));
        Assert.assertFalse(userRepository.isEmailExist(email));
        Assert.assertFalse(userRepository.existsById(userId));
        Assert.assertEquals(expectedCount, userRepository.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        int expectedCount = NUMBER_OF_USER - 1;
        Assert.assertEquals(NUMBER_OF_USER, userRepository.getSize());

        @NotNull final User userForRemove = userRepository.findOneByIndex(0);
        @NotNull final String login = userForRemove.getLogin();
        @NotNull final String email = userForRemove.getEmail();
        @NotNull final String userId = userForRemove.getId();
        userRepository.removeOneByIndex(0);

        Assert.assertFalse(userRepository.isLoginExist(login));
        Assert.assertFalse(userRepository.isEmailExist(email));
        Assert.assertFalse(userRepository.existsById(userId));
        Assert.assertEquals(expectedCount, userRepository.getSize());
    }

    @After
    public void testRemoveAll() {
        System.out.println("AFTER");
        userRepository.removeAll();
        Assert.assertEquals(0, userRepository.getSize());
    }

}
