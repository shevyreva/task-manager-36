package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.service.IProjectService;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.repository.ProjectRepository;

import java.util.UUID;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private IProjectService projectService;

    @Before
    public void initService() {
        System.out.println("BEFORE");
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project " + i);
            project.setDescription("description" + i);
            if (i < 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
        }
        projectService = new ProjectService(projectRepository);
    }

    @Test
    public void testAddProject() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES + 1;

        @NotNull final String name = "project";
        @NotNull final String description = "description";
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final Project project = new Project();

        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectService.add(userId, project);

        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(null, project));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.add(userId, null));

        Assert.assertEquals(expectedCount, projectService.getSize());
    }

    @Test
    public void testCreateProject() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "project";
        @NotNull final String description = "description";
        @NotNull final String userId = UUID.randomUUID().toString();
        projectService.create(userId, name, description);

        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(null, name, description));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(userId, null, description));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.create(userId, name, null));

        Assert.assertEquals(expectedCount, projectService.getSize());
    }

    @Test
    public void testFindAll() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.findAll().size());
        final int actualOne = projectService.findAll(ProjectSort.BY_NAME.getComparator()).size();
        final int actualTwo = projectService.findAll(ProjectSort.BY_CREATED.getComparator()).size();
        final int actualThree = projectService.findAll(ProjectSort.BY_STATUS.getComparator()).size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualTwo);
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualOne);
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualThree);
    }

    @Test
    public void testFindAllForUser() {
        final int userCount = 4;
        Assert.assertEquals(userCount, projectService.findAll(USER_ID_1).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll((String) null));
    }

    @Test
    public void testFindByProjectId() {
        @NotNull final Project newProject = new Project();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final String projectId = newProject.getId();
        @NotNull final String incorrectProjectId = UUID.randomUUID().toString();

        newProject.setUserId(userId);
        newProject.setName("newProject");
        newProject.setDescription("newDescription");
        projectService.add(newProject);
        Assert.assertNotNull(projectService.findOneById(userId, projectId));

        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(null, projectId));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(userId, null));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(incorrectUserId, projectId));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(userId, incorrectProjectId));
    }

    @Test
    public void testFindByProjectIndex() {
        @NotNull final Project newProject = new Project();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();

        newProject.setUserId(userId);
        newProject.setName("newProject");
        newProject.setDescription("newDescription");
        projectService.add(newProject);
        @NotNull final Integer index = 0;
        @NotNull final Integer incorrectIndex = 666;
        Assert.assertNotNull(projectService.findOneByIndex(userId, index));

        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(null, index));
        Assert.assertThrows(IndexEmptyException.class, () -> projectService.findOneByIndex(userId, null));
        Assert.assertThrows(IndexEmptyException.class, () -> projectService.findOneByIndex(userId, incorrectIndex));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneByIndex(incorrectUserId, index));
    }

    @Test
    public void testRemoveOne() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull final Project removeProject = projectService.findOneByIndex(0);
        @NotNull final String id = removeProject.getId();
        projectService.removeOne(removeProject);

        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(id));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOne(null));

        Assert.assertEquals(expectedCount, projectService.getSize());
    }

    @Test
    public void testRemoveOneForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull final Project removeProject = projectService.findOneByIndex(0);
        @NotNull final String id = removeProject.getId();
        @NotNull final String userId = removeProject.getUserId();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        projectService.removeOne(userId, removeProject);

        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(id));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOne(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeOne(null, removeProject));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOne(incorrectUserId, removeProject));

        Assert.assertEquals(expectedCount, projectService.getSize());
    }

    @Test
    public void testRemoveById() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull Project removeProject = projectRepository.findOneByIndex(0);
        @NotNull final String id = removeProject.getId();
        @NotNull final String incorrectId = UUID.randomUUID().toString();
        projectService.removeOneById(id);

        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(id));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOneById(incorrectId));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeOneById(null));

        Assert.assertEquals(expectedCount, projectService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull Project removeProject = projectRepository.findOneByIndex(0);
        @NotNull final String id = removeProject.getId();
        @NotNull final String userId = removeProject.getUserId();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final String incorrectId = UUID.randomUUID().toString();
        projectService.removeOneById(userId, id);

        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(id));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeOneById(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeOneById(null, id));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOneById(incorrectUserId, id));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOneById(userId, incorrectId));

        Assert.assertEquals(expectedCount, projectService.getSize());
    }


    @Test
    public void testRemoveByIndex() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull Project removeProject = projectRepository.findOneByIndex(0);
        @NotNull final String id = removeProject.getId();
        @NotNull final Integer incorrectIndex = 666;
        projectService.removeOneByIndex(0);

        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(id));
        Assert.assertThrows(IndexEmptyException.class, () -> projectService.removeOneByIndex(null));
        Assert.assertThrows(IndexEmptyException.class, () -> projectService.removeOneByIndex(incorrectIndex));

        Assert.assertEquals(expectedCount, projectService.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        int expectedCount = NUMBER_OF_ENTRIES - 1;

        @NotNull Project removeProject = projectRepository.findOneByIndex(0);
        @NotNull final String id = removeProject.getId();
        @NotNull final String userId = removeProject.getUserId();
        @NotNull final String incorrectUserId = UUID.randomUUID().toString();
        @NotNull final Integer incorrectIndex = 666;
        projectService.removeOneByIndex(0);

        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findOneById(id));

        Assert.assertThrows(IndexEmptyException.class, () -> projectService.removeOneByIndex(userId, incorrectIndex));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeOneByIndex(null, 0));
        Assert.assertThrows(IndexEmptyException.class, () -> projectService.removeOneByIndex(userId, null));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.removeOneByIndex(incorrectUserId, 0));

        Assert.assertEquals(expectedCount, projectService.getSize());
    }

    @Test
    public void testUpdateById() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newDescription";
        @NotNull final Project updateProject = projectService.findOneByIndex(0);
        @NotNull final String userId = updateProject.getUserId();
        @NotNull final String id = updateProject.getId();

        projectService.updateById(userId, id, newName, newDescription);
        Assert.assertEquals(newName, updateProject.getName());
        Assert.assertEquals(newDescription, updateProject.getDescription());

        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userId, null, newName, newDescription));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, id, newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userId, id, null, newDescription));
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String newName = "newName";
        @NotNull final String newDescription = "newDescription";
        @NotNull final Project updateProject = projectService.findOneByIndex(0);
        @NotNull final String userId = updateProject.getUserId();

        projectService.updateByIndex(userId, 0, newName, newDescription);
        Assert.assertEquals(newName, updateProject.getName());
        Assert.assertEquals(newDescription, updateProject.getDescription());

        Assert.assertThrows(IndexEmptyException.class, () -> projectService.updateByIndex(userId, null, newName, newDescription));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(null, 0, newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(userId, 0, null, newDescription));
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final Project updateProject = projectService.findOneByIndex(0);
        @NotNull final String userId = updateProject.getUserId();
        @NotNull final String id = updateProject.getId();
        @NotNull final Status completed = Status.COMPLETED;
        @NotNull final Status progress = Status.IN_PROGRESS;
        @NotNull final Status notStarted = Status.NON_STARTED;

        projectService.changeProjectStatusById(userId, id, completed);
        Assert.assertEquals(completed, updateProject.getStatus());

        projectService.changeProjectStatusById(userId, id, progress);
        Assert.assertEquals(progress, updateProject.getStatus());

        projectService.changeProjectStatusById(userId, id, notStarted);
        Assert.assertEquals(notStarted, updateProject.getStatus());

        Assert.assertThrows(StatusNotFoundException.class, () -> projectService.changeProjectStatusById(userId, id, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userId, null, completed));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, id, completed));
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final Project updateProject = projectService.findOneByIndex(0);
        @NotNull final String userId = updateProject.getUserId();
        @NotNull final Status completed = Status.COMPLETED;
        @NotNull final Status progress = Status.IN_PROGRESS;
        @NotNull final Status notStarted = Status.NON_STARTED;

        projectService.changeProjectStatusByIndex(userId, 0, completed);
        Assert.assertEquals(completed, updateProject.getStatus());

        projectService.changeProjectStatusByIndex(userId, 0, progress);
        Assert.assertEquals(progress, updateProject.getStatus());

        projectService.changeProjectStatusByIndex(userId, 0, notStarted);
        Assert.assertEquals(notStarted, updateProject.getStatus());

        Assert.assertThrows(StatusNotFoundException.class, () -> projectService.changeProjectStatusByIndex(userId, 0, null));
        Assert.assertThrows(IndexEmptyException.class, () -> projectService.changeProjectStatusByIndex(userId, null, completed));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(null, 0, completed));
    }

    @After
    public void testClearProjects() {
        System.out.println("AFTER");
        projectService.removeAll();
        Assert.assertEquals(0, projectService.getSize());
    }

}
