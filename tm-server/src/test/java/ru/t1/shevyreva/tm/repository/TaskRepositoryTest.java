package ru.t1.shevyreva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void initRepository() {
        System.out.println("BEFORE");
        taskRepository = new TaskRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task " + i);
            task.setDescription("description" + i);
            if (i < 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            taskRepository.add(task);
        }
    }

    @Test
    public void testAddTask() {
        int expectedCount = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task newTask = new Task();
        taskRepository.add(newTask);
        Assert.assertEquals(expectedCount, taskRepository.getSize());
    }

    @Test
    public void testAddActualTask() {
        int expectedCount = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "ActualName";
        @NotNull final String description = "ActualDescription";
        @NotNull final Task newTask = new Task();

        newTask.setName(name);
        newTask.setDescription(description);
        taskRepository.add(userId, newTask);
        Assert.assertEquals(expectedCount, taskRepository.getSize());

        @Nullable final Task actualTask = taskRepository.findOneByIndex(taskRepository.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testCreateTask() {
        int expectedCount = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull Task task_one = taskRepository.create(userId, "CreateTaskName");
        Assert.assertEquals(expectedCount, taskRepository.findAll().size());

        @NotNull Task task_two = taskRepository.create(userId, "CreateTaskName2", "TaskDescription");
        Assert.assertEquals(expectedCount + 1, taskRepository.getSize());

        @NotNull final String expectedName = task_one.getName();
        @NotNull final String actualTaskOneName = taskRepository.findOneById(task_one.getId()).getName();
        Assert.assertEquals(expectedName, actualTaskOneName);

        @NotNull final String expectedDescription = task_two.getDescription();
        @NotNull final String actualTaskTwoDescription = taskRepository.findOneById(task_two.getId()).getDescription();
        Assert.assertEquals(expectedDescription, actualTaskTwoDescription);
    }

    @Test
    public void testFindAll() {
        int actualSize = taskRepository.getSize();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualSize);

        @NotNull final Task task = new Task();
        task.setName("FindAll");
        task.setDescription("FindAll");
        taskRepository.add(task);

        int expectedCount = NUMBER_OF_ENTRIES + 1;
        actualSize = taskRepository.getSize();
        Assert.assertEquals(expectedCount, actualSize);
    }

    @Test
    public void testFindAllComparator() {
        int actualSizeOne = taskRepository.findAll(TaskSort.BY_NAME.getComparator()).size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualSizeOne);
        int actualSizeTwo = taskRepository.findAll(TaskSort.BY_CREATED.getComparator()).size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualSizeTwo);
        int actualSizeThree = taskRepository.findAll(TaskSort.BY_STATUS.getComparator()).size();
        Assert.assertEquals(NUMBER_OF_ENTRIES, actualSizeThree);
    }

    @Test
    public void testFindByTaskId() {
        @NotNull Task newTask = new Task();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "FindById";
        @NotNull final String description = "FindById";

        newTask.setName(name);
        newTask.setUserId(userId);
        newTask.setDescription(description);
        taskRepository.add(newTask);

        @NotNull final Task findTaskById = taskRepository.findOneById(newTask.getId());
        @NotNull final Task findTaskByUserId = taskRepository.findOneById(userId, newTask.getId());
        Assert.assertNotNull(findTaskById);
        Assert.assertNotNull(findTaskByUserId);

        @NotNull final String expectedName = newTask.getName();
        Assert.assertEquals(expectedName, findTaskById.getName());
        Assert.assertEquals(expectedName, findTaskByUserId.getName());
    }

    @Test
    public void testFindByTaskIndex() {
        @NotNull Task newTask = new Task();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "FindByIndex";
        @NotNull final String description = "FindByIndex";

        newTask.setName(name);
        newTask.setUserId(userId);
        newTask.setDescription(description);
        taskRepository.add(newTask);

        @NotNull final Integer index = NUMBER_OF_ENTRIES;
        @NotNull final Task findTaskByIndex = taskRepository.findOneByIndex(index);
        @NotNull final Task findTaskByUserId = taskRepository.findOneByIndex(userId, 0);
        Assert.assertNotNull(findTaskByIndex);
        Assert.assertNotNull(findTaskByUserId);

        @NotNull final String expectedName = newTask.getName();
        Assert.assertEquals(expectedName, findTaskByIndex.getName());
        Assert.assertEquals(expectedName, findTaskByUserId.getName());
    }

    @Test
    public void testFindForUser() {
        Assert.assertEquals(4, taskRepository.findAll(USER_ID_1).size());
        Assert.assertEquals(6, taskRepository.findAll(USER_ID_2).size());

        @NotNull Task newTask = new Task();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "FindByUser";
        @NotNull final String description = "FindByUser";

        newTask.setUserId(userId);
        newTask.setName(name);
        newTask.setDescription(description);
        taskRepository.add(newTask);

        @NotNull final List<Task> Tasks = taskRepository.findAll(userId);
        for (@NotNull final Task Task : Tasks) {
            Assert.assertEquals(newTask.getUserId(), Task.getUserId());
        }
    }

    @Test
    public void testFindTaskByProjectId() {
        @NotNull Task findTaskOne = taskRepository.findOneByIndex(0);
        @NotNull Task findTaskTwo = taskRepository.findOneByIndex(1);

        @NotNull final String projectId = UUID.randomUUID().toString();
        findTaskOne.setProjectId(projectId);
        findTaskTwo.setProjectId(projectId);

        @NotNull List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_1, projectId);
        for (@NotNull final Task task : tasks) {
            Assert.assertNotNull(task);
            Assert.assertEquals(projectId, task.getProjectId());
        }
    }

    @Test
    public void testRemoveOne() {
        @NotNull Task findTask = taskRepository.findOneByIndex(0);
        int expected = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
        taskRepository.removeOne(findTask);
        Assert.assertEquals(expected, taskRepository.getSize());
        Assert.assertNull(taskRepository.findOneById(findTask.getId()));
    }

    @Test
    public void testRemoveById() {
        @NotNull Task findTask = taskRepository.findOneByIndex(0);
        int expected = NUMBER_OF_ENTRIES - 1;
        @NotNull final String id = findTask.getId();
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
        taskRepository.removeOneById(id);
        Assert.assertEquals(expected, taskRepository.getSize());
        Assert.assertNull(taskRepository.findOneById(id));
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Task findTask = taskRepository.findOneByIndex(0);
        int expected = NUMBER_OF_ENTRIES - 1;
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
        taskRepository.removeOneByIndex(0);
        Assert.assertEquals(expected, taskRepository.getSize());
        Assert.assertNull(taskRepository.findOneById(findTask.getId()));
    }

    @Test
    public void testRemoveForUser() {
        int userTaskCount = taskRepository.findAll(USER_ID_1).size();
        int expected = NUMBER_OF_ENTRIES - userTaskCount;
        @Nullable final List<Task> Tasks = taskRepository.findAll(USER_ID_1);
        taskRepository.removeAll(USER_ID_1);
        for (@NotNull final Task Task : Tasks) {
            Assert.assertNull(taskRepository.findOneById(Task.getId()));
        }
        Assert.assertEquals(expected, taskRepository.getSize());
    }

    @After
    public void testClearTasks() {
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        System.out.println("AFTER");
    }

}
