package ru.t1.shevyreva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.shevyreva.tm.exception.field.TaskIdEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.ProjectRepository;
import ru.t1.shevyreva.tm.repository.TaskRepository;

import java.util.UUID;

public class ProjectTaskServiceTest {

    private final int NUMBER_OF_ENTRIES = 5;

    private final String USER_ID_1 = UUID.randomUUID().toString();

    private final String USER_ID_2 = UUID.randomUUID().toString();

    ITaskRepository taskRepository;

    IProjectRepository projectRepository;

    IProjectTaskService projectTaskService;

    @Before
    public void initService() {
        System.out.println("BEFORE");
        taskRepository = new TaskRepository();
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("task " + i);
            task.setDescription("description" + i);
            @NotNull final Project project = new Project();
            project.setName("project " + i);
            project.setDescription("description" + i);
            if (i < 3) {
                task.setUserId(USER_ID_1);
                project.setUserId(USER_ID_1);
            } else {
                task.setUserId(USER_ID_2);
                project.setUserId(USER_ID_2);
            }
            taskRepository.add(task);
            projectRepository.add(project);
        }
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final Project project = projectRepository.findOneByIndex(0);
        @NotNull final Task task = taskRepository.findOneByIndex(0);
        @NotNull final String projectId = project.getId();
        @NotNull final String taskId = task.getId();
        @NotNull final String incorrectId = UUID.randomUUID().toString();

        Assert.assertNull(task.getProjectId());
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(projectId, task.getProjectId());

        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(null, projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, null, taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, projectId, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(incorrectId, projectId, taskId));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, incorrectId, taskId));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(USER_ID_1, projectId, incorrectId));
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final Project project = projectRepository.findOneByIndex(0);
        @NotNull final Task task = taskRepository.findOneByIndex(0);
        @NotNull final String projectId = project.getId();
        @NotNull final String taskId = task.getId();
        @NotNull final String incorrectId = UUID.randomUUID().toString();
        task.setProjectId(projectId);

        Assert.assertNotNull(task.getProjectId());
        projectTaskService.unbindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertNull(task.getProjectId());
        Assert.assertNotEquals(projectId, task.getProjectId());

        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(null, projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(USER_ID_1, null, taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskToProject(USER_ID_1, projectId, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskToProject(incorrectId, projectId, taskId));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskToProject(USER_ID_1, incorrectId, taskId));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskToProject(USER_ID_1, projectId, incorrectId));
    }

    @Test
    public void removeByProjectId() {
        @NotNull final Project project = projectRepository.findOneByIndex(0);
        @NotNull final Task task = taskRepository.findOneByIndex(0);
        @NotNull final String taskId = task.getId();
        @NotNull final String projectId = project.getId();
        @NotNull final String incorrectId = UUID.randomUUID().toString();
        task.setProjectId(projectId);

        projectTaskService.removeByProjectId(USER_ID_1, projectId);
        Assert.assertNull(taskRepository.findOneById(taskId));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, projectId));

        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeByProjectId(null, projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeByProjectId(USER_ID_1, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeByProjectId(incorrectId, projectId));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.removeByProjectId(USER_ID_1, incorrectId));
    }

    @After
    public void testRemoveAll() {
        System.out.println("AFTER");
        taskRepository.removeAll();
        projectRepository.removeAll();
    }
}
