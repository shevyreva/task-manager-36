package ru.t1.shevyreva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    private Role role = null;

    @NotNull
    private Date date = new Date();

}
