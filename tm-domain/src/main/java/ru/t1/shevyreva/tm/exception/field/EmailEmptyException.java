package ru.t1.shevyreva.tm.exception.field;

public class EmailEmptyException extends AbsrtactFieldException {

    public EmailEmptyException() {
        super("Error! Email is empty.");
    }

}
