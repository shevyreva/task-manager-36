package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectCompleteStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Complete project by Id.";

    @NotNull
    private final String NAME = "project-update-status-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        getProjectEndpoint().changeProjectStatusById(request);
    }

}
