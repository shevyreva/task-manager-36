package ru.t1.shevyreva.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.service.ITokenService;

@Getter
@Setter
public class TokenService implements ITokenService {

    @NotNull
    private String token;
}
