package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.user.UserRemoveRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserRemoveByEmail extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "User remove by email.";

    @NotNull
    private final String NAME = "user-remove-by-email";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY EMAIL]:");
        System.out.println("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRemoveRequest request = new UserRemoveRequest();
        request.setEmail(email);
        request.setUserId(null);
        request.setLogin(null);
        getUserEndpoint().removeUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
