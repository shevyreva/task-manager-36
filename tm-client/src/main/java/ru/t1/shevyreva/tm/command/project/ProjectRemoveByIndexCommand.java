package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Remove project by Index.";

    @NotNull
    private final String NAME = "project-remove-by-index";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE BY INDEX]");
        System.out.println("Enter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(getToken(), index);
        getProjectEndpoint().removeProjectByIndex(request);
    }

}
